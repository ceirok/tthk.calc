﻿namespace Calc.Core.ServiceInterfaces.Operators
{
    public interface IOperator : IExpressionNode
    {
        string Symbol { get; }
        int Priority { get; }

    }
}
