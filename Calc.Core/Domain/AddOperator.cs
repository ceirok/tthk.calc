﻿using Calc.Core.ServiceInterfaces.Operators;
using System;

namespace Calc.Core.Domain
{
    public class AddOperator : IBinaryOperator
    {
        public string Symbol => "+";

        public int Priority => 1;

        public decimal Exec(decimal left, decimal right)
        {
            return left + right;
        }
    }
}
