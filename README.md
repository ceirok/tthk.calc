# README #



## Kuidas repoot kasutada? ##

* Tehke endale fork
* Oma repos, mis te forkisite, ärge masterisse ise midagi pushige, et vältida tulevikus merge konflikte


## 6. nädal (3.10.2018)

### Ülesanne

* Avalides parsimine peab aru saama mitmetest alam-avaldisest samal tasemel. nt: 1 + (1+2) * (3+1)

## 5. nädal (26.09.2018)

* yield return
* generikud
* sulgude parsimine (lihtsustatud)

## 4. nädal (19.09.2018) ##

* yield return
* anonyymsed funktsioonid

### Ülesanne ###

* Avaldise stringi parsimine puu struktuuriks (pushige oma töö oma reposse ja saatke mulle link)

## 3. nädal (12.09.2018) ##

### Ülesanne ###

* Puust avaldise stringi koostamine

### kasulikud lingid ###

* [Visual studio keyboard shortcuts](https://docs.microsoft.com/en-us/visualstudio/ide/default-keyboard-shortcuts-in-visual-studio?view=vs-2017)